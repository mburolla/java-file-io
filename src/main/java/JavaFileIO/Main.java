package JavaFileIO;

public class Main {
    public static void main(String[] args) {
        var worker =  new Worker();
        // Writes...
        worker.createFile();
        worker.fileWriter();
        worker.bufferedWriter();

        // Reads...
        worker.fileReadWithScanner();
        worker.fileReader();
        worker.inputStream();
    }
}
