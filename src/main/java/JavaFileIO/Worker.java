package JavaFileIO;

import java.io.*;
import java.util.Scanner;

public class Worker {

    //
    // Constructors
    //

    public Worker() { }

    //
    // Public Write Methods
    //

    public void createFile() {
        try {
            var file = new File("output-1.txt");
            file.createNewFile();
            System.out.println(file.length());
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public void fileWriter() {
        try {
            try (var fileWriter = new FileWriter("output-2.txt")) { // Extends OuptutStreamWriter which extends Writer which implements Closable.
                fileWriter.write("This is a test.");
            }
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public void bufferedWriter() {
        try {
            try (var bufferedWriter = new BufferedWriter(new FileWriter("output-3.txt"))) { // Extends extends Writer which implements Closable.
                bufferedWriter.write("This is a test.");
            }
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    //
    // Public Read Methods
    //

    public void fileReadWithScanner() {
        try {
            String inputLine;
            var file = new File("input-1.txt"); // Does not implement Closable interface.
            try (Scanner scanner = new Scanner(file)) {
                while (scanner.hasNextLine()) {
                    inputLine = scanner.nextLine();
                    // var arr = inputLine.split(" ");
                    System.out.println(inputLine);
                }
                // scanner.close(); // No need to do this because of try-with-resources.
            }
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public void fileReader() {
        try {
            char[] buffer = new char[10];
            try (var fileReader = new FileReader("input-1.txt")) {
                var c = fileReader.read(buffer);
                System.out.println(buffer);
            }
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public void inputStream() {
        try {
            String line;
            File file = new File("input-1.txt");
            StringBuilder stringBuilder = new StringBuilder();
            InputStream inputStream = new FileInputStream(file);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line + System.lineSeparator());
            }
            System.out.println(stringBuilder);
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
